import ScrollBar from "react-scrollbars-custom";
import { TweenMax } from "gsap";

class About extends React.Component {

	constructor(props){
	super(props);
		this.state = {
			introLoad: 0,
		}
	}

	componentDidMount(){
		if (this.state.introLoad === 0){
			TweenMax.fromTo('#aboutImage', 2, { x: "-100%" }, { x:0, ease: "sine.out" });
			TweenMax.fromTo('#aboutContent', 1.5, { opacity: 0 }, { opacity:1, ease: "sine.out" }).delay(.75);
			this.setState({introLoad : 1});
		}
	}

	render(){
		return (
			<section className="grid-container fluid Section__main section">
				<div className="align-stretch grid-x h100">
					<div id="aboutImage" className="cell small-12 xlarge-6 grid-x bgImage__cover bgImage small-order-2 xlarge-order-1 show-for-large" style={{backgroundImage:'url('+ this.props.data[0].acf.main_image +')'}}>
					</div>
					<div id="aboutContent" className="cell small-12 xlarge-6 grid-x align-top align-center small-order-1 xlarge-order-2">
						<ScrollBar style={{ width: "100%", height: "100%" }}>
							<div className="small-12 grid-x cell align-center">
			    				<div className="cell small-8 text-center pt5 pb5 grid-x ">
			    					<h2 className="heading heading__xxl heading__caps cell small-12">{this.props.data[0].title.rendered}</h2>
			    					<hr className="ruler__dark cell small-12"/>
			    					<div dangerouslySetInnerHTML={{__html:this.props.data[0].acf.content}} className="pt2 cell small-12"/>
			    					<div className="pt2 pb2 cell small-offset-1 small-10 medium-offset-2 medium-8">
			    						<img src={this.props.data[0].acf.small_image} className="grayscale"/>
			    					</div>
			    					{this.props.data[0].acf.skills
			    						.map(skillKey => {
			    							return (
			    								<div className="small-12 cell pb1" key={skillKey.id}>
			    									<h6 className="heading__md heading__caps">{skillKey.category}</h6>
			    									<div className="small-12 cell grid-x small-up-1 medium-up-2 large-up-3" key={skillKey.id}>
				    									{skillKey.technology.map(skillItem => {
				    										return(
				    											<div className="cell">
				    											{skillItem.title}
				    											</div>
				    										)
				    									})}
			    									</div>
			    								</div>
			    							)
			    						})
			    					}
			    				</div>
							</div>
						</ScrollBar>
					</div>
				</div>
			</section>
		)
	}
}

export default About