import * as emailjs from 'emailjs-com';

import Input from './Input';

class ContactForm extends React.Component{
	state = {
		contactForm:{
			name: {
				elementType: 'input',
				elementConfig: {
					type: 'text',
					placeholder: 'Name',
				},
				value: '',
				validation:{
					required: true,
				},
				valid: false,
				touched: false,
				errorMessage: 'Please enter a valid name'
			},
			email: {
				elementType: 'input',
				elementConfig: {
					type: 'email',
					placeholder: 'Email',
				},
				value: '',
				validation:{
					required: true,
					email: true
				},
				valid: false,
				touched: false,
				errorMessage: 'Please enter a valid email address'
			},
			message: {
				elementType: 'textarea',
				elementConfig: {
					placeholder: 'Message',
				},
				value: '',
				validation:{
					required: true,
					minLength: 25
				},
				valid: false,
				touched: false,
				errorMessage: 'Your message must be a minimum of 30 characters'
			},
		},
		formIsValid:false,
		formMessageSuccess: 'Your message has been successfully sent.',
		formMessageFail: 'Unfortunately your message could not be sent. Please email info@robclarkdesigner.co.uk',
		formSuccess: false,
		formFail:false
	}

	submitForm = (event) =>{
		event.preventDefault();
		const formData = {};
		for (let formElementIdentifier in this.state.contactForm){
			formData[formElementIdentifier] = this.state.contactForm[formElementIdentifier].value;
		}

		emailjs.send('rob_clark_designer', 'template_SuHg95xb', formData, 'user_KusB8wjxaLrPLutCiddLG')
	    .then((response, inputIdentifier)  => {
	    	this.setState({formSuccess: true, formFail: false, formIsValid: false});
	       console.log('SUCCESS!', response.status, response.text);
	    }, (error) => {
	    	this.setState({formSuccess: false, formFail: true});
	       console.log('FAILED...', error);
	    });
	}

	checkValidation(value, rules){
		let isValid = true

		if (rules.required){
			isValid = value.trim() !== '' && isValid;
		}

		if (rules.minLength){
			isValid = value.length >= rules.minLength && isValid;
		}

		if (rules.maxLength){
			isValid = value.length <= rules.minLength && isValid;
		}

		if (rules.email){
			isValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
		}

		return isValid;
	}

	inputChangedHandler = (event, inputIdentifier) => {
		const updatedContactForm = {
			...this.state.contactForm
		};
		const updatedFormElement = {
			...updatedContactForm[inputIdentifier]
		};
		updatedFormElement.value = event.target.value;
		updatedFormElement.valid = this.checkValidation(updatedFormElement.value, updatedFormElement.validation);
		updatedFormElement.touched = true;
		updatedContactForm[inputIdentifier] = updatedFormElement;
		

		let formIsValid = true;
		for (let inputIdentifier in updatedContactForm){
			formIsValid = updatedContactForm[inputIdentifier].valid && formIsValid;
		}

		this.setState({contactForm:updatedContactForm, formIsValid: formIsValid});
	}



	render(){

		const formElementsArray = [];
		for (let key in this.state.contactForm){
			formElementsArray.push({
				id: key,
				config: this.state.contactForm[key],
			})
		}

		return(
			<React.Fragment>
				<h6 className="heading heading__sm heading__body-font font300 contactheading">Send me a message @</h6>
				<a className="heading heading__sm heading__body-font font700 heading__dark contactheading" href="mailto:info@robclarkdesigner.co.uk">info@robclarkdesigner.co.uk</a>
				<h6 className="heading heading__sm heading__body-font font300 pb1 pt2 contactheading">Send me a message @</h6>
				<form id="contactForm" onSubmit={this.submitForm}>
					{formElementsArray.map(formElement => (
						<Input 
							key={formElement.id}
							elementType={formElement.config.elementType}
							elementConfig={formElement.config.elementConfig}
							value={formElement.config.value}
							invalid={!formElement.config.valid}
							shouldValidate={formElement.config.validation}
							touched={formElement.config.touched}
							changed={(event) => this.inputChangedHandler(event, formElement.id)}
							errorMessage={formElement.config.errorMessage}/>
					))}
					<div>
						<button className="hollow button primary" disabled={!this.state.formIsValid}>Contact Me</button>
					</div>
					<div className="formSuccess">
						{this.state.formSuccess ? this.state.formMessageSuccess : null}
					</div>
					<div className="formFail">
						{this.state.formFail ? this.state.formMessageFail : null}
					</div>
				</form>
			</React.Fragment>
		)
	}
}

export default ContactForm