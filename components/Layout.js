import Router from 'next/router';
import nprogress from 'nprogress';
import Link from "next/link";
import axios from 'axios';

import "../styles.scss";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';
import { TweenMax } from "gsap";

import MenuIcon from './MenuIcon'
import Menu from './Menu'

library.add(fab)

Router.onRouteChangeStart = url => {
	nprogress.start();
}

Router.onRouteChangeComplete = () => {
	nprogress.done();
	TweenMax.fromTo('#root', 1.5, { opacity: 1 }, { opacity: 0, ease: "power2.inOut" }).yoyo(true);
}

Router.onRouteChangeError = () => {
	nprogress.done();
}


class Layout extends React.Component{
	constructor(props){
		super(props);
		this.state= {
			menuOpen : false,
			contactBoxOpen : false,
			social: []
		}
		this.MenuToggle = this.MenuToggle.bind(this);
		this.MenuClose = this.MenuClose.bind(this);
		this.MenuAnimation = this.MenuAnimation.bind(this);
	}

	componentDidMount(){ 
		axios.get('https://cms.robclarkdesigner.co.uk/wp-json/acf/v3/options/options')
			.then((optionsResponse) => {
				this.setState({social: optionsResponse.data.acf.social_icons});
			})
	}

	MenuAnimation(){
		if (this.state.menuOpen === true){
			TweenMax.to('.menu', .5, { autoAlpha: 0 });
		} else if (this.state.contactBoxOpen === false){
			TweenMax.to('.menu', .5, { autoAlpha: 1 });
			TweenMax.fromTo('#menuContact', 1.25, { x: '100%' }, { x: '0%', ease: "power2.inOut" });
		} else {
			TweenMax.to('.menu', .5, { autoAlpha: 1 });
		}
	}

	MenuToggle(){
		this.setState(state => ({
			menuOpen : !state.menuOpen,
			contactBoxOpen : true,
		}));
		this.MenuAnimation();
		
	}

	MenuClose(){
		this.setState(state => ({
			menuOpen : false,
		}));
		this.MenuAnimation();
	}

	render(){
		return(
			<div className="root">
				<Menu menuClose = {this.MenuClose}/>
				<img src="https://firebasestorage.googleapis.com/v0/b/rob-clark-designer.appspot.com/o/rc-logo.svg?alt=media&token=ccca1a57-f0f6-41e7-856d-8eea16aeb080" id="rcLogo"/>
				<React.Fragment>
					<nav className="pb3 pl1 pr1 pt2 Section__Dark Layout grid-x align-stretch">
						<div className="grid-x align-self-top small-12 align-center">
							<MenuIcon
								menuOpen = {this.state.menuOpen}
								menuToggle = {this.MenuToggle}
							/>
						</div>
						<div className="grid-x align-self-bottom small-12 social hide-for-small-only">
							{this.state.social
								.map(socialKey => {
									return (<a className="small-12 cell text-center Layout__Link pb1 align-self-bottom" href={socialKey.link} target="_blank">
											<FontAwesomeIcon icon={['fab', socialKey.platform.toLowerCase()]}/>
										</a>
									)
								})
							}
							<a className="small-12 cell text-center Layout__Link pb1 align-self-bottom" href="mailto:info@robclarkdesigner.co.uk">
								<FontAwesomeIcon icon={faEnvelope}/>
							</a>
						</div>
					</nav>
				</React.Fragment>
				{this.children}
			</div>
	)
	}
	
}

export default Layout;