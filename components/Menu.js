import axios from 'axios';
import Link from 'next/link';
import { withRouter } from 'next/router'

import Contact from '../components/Contact';

class Menu extends React.Component{
	constructor(props){
		super(props)

		this.state = {
			menu:[]
		}
	}



	componentDidMount(){ 
		axios.get('https://cms.robclarkdesigner.co.uk/wp-json/menus/v1/menus/main-menu')
			.then((menuResponse) => {
				this.setState({menu: menuResponse.data.items});
			})
	}

	render(){
		return(
			<React.Fragment>
				<div className="menu h100 grid-container fluid">
					<div className="grid-x">
						<div className="cell small-12 xlarge-6 grid-y align-middle align-center text-center" id="menuItems">
							<Link href="/">
								<a className="heading__xxl heading__dark heading__caps heading__heading-font pb1 pt1 font700 cell">Home</a>
							</Link>
							{this.state.menu
								.filter(menuKey => menuKey.object_id != 24)
								.map(menuKey => {
									return(
										<Link href="/[slug]" key={menuKey.ID} as={`/${menuKey.slug}`}>
										<a className="heading__xxl heading__dark heading__caps heading__heading-font pb1 pt1 font700 cell" onClick={this.props.menuClose}>{menuKey.title}</a>
										</Link>
									)
								})
							}
						</div>
						<div className="cell small-12 xlarge-6 Section__Dark grid-x align-middle align-center show-for-xlarge" id="menuContact">
							<div className="cell small-5 text-left heading__primary">
								<Contact/>
							</div>
						</div>
					</div>
				</div>
			</React.Fragment>
		)
	}
}

export default withRouter(Menu);