const MenuIcon = (props) => {
	return(
		<React.Fragment>
			<div className={props.menuOpen ? 'menuIcon open': 'menuIcon'} onClick={props.menuToggle}>
				<span></span>
				<span></span>
				<span></span>
			</div>
		</React.Fragment>
	)
}

export default MenuIcon