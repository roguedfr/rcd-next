import { TweenMax } from "gsap";
import Link from 'next/link';
import SectionLink from '../components/SectionLink';

class Portfolio extends React.Component {

	constructor(props){
	super(props);
		this.state = {
			introLoad: 0,
		}
	}

	componentDidMount(){
		if (this.state.introLoad === 0){
			TweenMax.fromTo('#portfolioDevelopment', 2, { x: "-100%" }, { x:0, ease: "sine.out" });
			TweenMax.fromTo('#portfolioDesigner', 2, { x: "100%" }, { x:0, ease: "sine.out" }).delay(.75);
			this.setState({introLoad : 1});
		}
	}

	render(){
		const aboutHover = () => {
	    	TweenMax.to('#portfolioDevelopment', 1, { css: {backgroundSize:"160%"}, ease: "sine.out"});
	    }
	    const aboutRelease = () => {
	    	TweenMax.to('#portfolioDevelopment', 1, { css: {backgroundSize:"150%"}, ease: "sine.out"});
	    }
	    const aboutDesignHover = () => {
	    	TweenMax.to('#portfolioDesigner', 1, { css: {backgroundSize:"160%"}, ease: "sine.out"});
	    }
	    const aboutDesignRelease = () => {
	    	TweenMax.to('#portfolioDesigner', 1, { css: {backgroundSize:"150%"}, ease: "sine.out"});
	    }
		return(
			<section className="grid-container fluid Section__main section">
				<div className="align-stretch grid-x h100">
					<div id="portfolioDevelopment" className="cell small-12 xxlarge-6 Section__Primary grid-x align-middle align-center portfolio__link h50" style={{backgroundImage:'url('+ this.props.data.acf.development_background + ')'}}>
						<div className="cell small-8 text-center grid-x align-middle align-center">
							<SectionLink
		    					title={this.props.data.acf.development_title}
		    					url={this.props.data.acf.development_link}
		    					hoverOver={aboutHover}
		    					hoverRelease={aboutRelease}
		    					internal = "true"
		    				/>
						</div>
					</div>
					<div id="portfolioDesigner" className="cell small-12 xxlarge-6 Section__Dark grid-x align-middle align-center portfolio__link h50" style={{backgroundImage:'url('+ this.props.data.acf.design_background + ')'}}>
						<div className="cell small-8 text-center grid-x align-middle align-center">
							<SectionLink
		    					title={this.props.data.acf.design_title}
		    					url={this.props.data.acf.design_link}
		    					hoverOver={aboutDesignHover}
		    					hoverRelease={aboutDesignRelease}
		    					internal = "true"
		    				/>
						</div>
					</div>
				</div>
			</section>
		)
	}
}

export default Portfolio;