import { TweenMax } from "gsap";
import Link from 'next/link';
import Slider from "react-slick";

import SectionLink from '../components/SectionLink';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLongArrowAltUp, faLongArrowAltDown } from "@fortawesome/free-solid-svg-icons";

class PortfolioList extends React.Component {

	constructor(props){
	super(props);
		this.state = {
			introLoad: 0,
			nav1: null,
	      	nav2: null
		}
		this.SlickNext = this.SlickNext.bind(this);
		this.SlickPrev = this.SlickPrev.bind(this);
	}

	componentDidMount(){
		if (this.state.introLoad === 0){
			TweenMax.fromTo('#portfolioDevelopment', 2, { x: "-100%" }, { x:0, ease: "sine.out" });
			TweenMax.fromTo('#portfolioDesigner', 2, { x: "100%" }, { x:0, ease: "sine.out" }).delay(.75);
			this.setState({introLoad : 1});
		}
		this.setState({
	      nav1: this.slider1,
	      nav2: this.slider2
	    });
	}

	SlickNext(){
		this.slider1.slickNext();
	}
	SlickPrev(){
		this.slider1.slickPrev();
	}

	render(){

		const settings = {
	      dots: false,
	      arrows:false,
	      infinite: true,
	      fade: true,
	      speed: 750,
	    };

	    const settingsText = {
	      dots: false,
	      arrows:false,
	      infinite: true,
	      speed: 750,
	      slidesToShow: 3,
	      vertical: true,
	      centerMode: true,
	      focusOnSelect: true,
	      responsive: [
	        {
	          breakpoint: 560,
	          settings: {
	            slidesToShow: 1,
	            centerMode: false,
	          }
	        },
	      ],
	    };

	    const portfolioHover = () => {
	    	TweenMax.to('.carouselItem img', 1, { scale: 1.2, ease: "sine.out", transformOrigin:"50% 50% 50%" });
	    }
	    const portfolioRelease = () => {
	    	TweenMax.to('.carouselItem img', 1, { scale: 1, ease: "sine.out", transformOrigin:"50% 50% 50%" });
	    }

		return(
			<section className="grid-container fluid Section__main section">
	    		<div className="align-stretch grid-x h100">
	    			<div className="cell small-12 large-6 grid-x portfolio_carousel h50">
	    				<Slider {...settings} ref={slider => (this.slider2 = slider)} asNavFor={this.state.nav1}>
	    					{this.props.data
	    						.filter(portfolioKey => portfolioKey.acf.portfolio_type.includes(this.props.pageInfo[0].slug))
	    						.sort((a, b) => parseInt(a.acf.priority) < parseInt(b.acf.priority))
	    						.map((portfolioKey, i) => {
	    							return(
	    								<React.Fragment>
	    								<div key={i} className="carouselItem"><img src={portfolioKey.acf.main_image}/></div>
	    								<SectionLink
					    					title="View"
					    					url={portfolioKey.slug}
					    					internal="true"
					    					hoverOver={portfolioHover}
					    					hoverRelease={portfolioRelease}
					    					template={this.props.pageInfo[0].template}
					    				/>
					    				</React.Fragment>
	    							)
	    						})}
	    				</Slider>
	    				
	    			</div>
	    			<div className="cell small-12 large-6 Section__Dark grid-x align-middle align-center h50 position-relative">
	    				<div className="cell small-8 text-center">
	    					<h2 className="heading heading__xxl heading__caps heading__primary">{this.props.pageInfo[0].title.rendered}</h2>
	    					<hr/>
	    					<div className="portfolio__carousel">
		    					<Slider {...settingsText} ref={slider => (this.slider1 = slider)} asNavFor={this.state.nav2}>
		    					{this.props.data
		    						.filter(portfolioKey => portfolioKey.acf.portfolio_type.includes(this.props.pageInfo[0].title.rendered.toLowerCase()))
		    						.sort((a, b) => parseInt(a.acf.priority) < parseInt(b.acf.priority))
		    						.map((portfolioKey, i) => {
		    							return(
		    								<div key={i} className="portfolio__item">
		    									<h3 className="heading heading__xl heading__caps heading__primary pt1">{portfolioKey.title.rendered}</h3>
		    									<h5 className="heading heading__sm heading__primary">{portfolioKey.acf.description}</h5>
		    								</div>
		    							)
		    						})}
		    					</Slider>
	    					</div>
	    				</div>
	    				<div className="sliderButtons">
    						<div id="sliderUp" className="mb1" onClick={this.SlickPrev}><FontAwesomeIcon icon={faLongArrowAltUp} /></div>
    						<div id="sliderDown" onClick={this.SlickNext}><FontAwesomeIcon icon={faLongArrowAltDown} /></div>
    					</div>
	    			</div>
	    		</div>
	    	</section>
		)
	}
}

export default PortfolioList;