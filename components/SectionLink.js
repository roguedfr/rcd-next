import Link from 'next/link';
import { withRouter } from 'next/router'


const SectionLink = (props) => {
	if ((props.internal === "true") && (props.template === "portfolio-list.php")){
		return(
			<Link href="/portfolio/[slug]" as={`/portfolio/${props.url}`}>
				<div className="sectionLink" onMouseOver={props.hoverOver} onMouseLeave={props.hoverRelease}>
					<span className="heading__xl heading__heading-font heading__caps font700">{props.title}</span>
				</div>
			</Link>
		)
	} else if (props.internal === "true"){
		return(
			<Link href="/[slug]" as={`/${props.url}`}>
				<div className="sectionLink" onMouseOver={props.hoverOver} onMouseLeave={props.hoverRelease}>
					<span className="heading__xl heading__heading-font heading__caps font700">{props.title}</span>
				</div>
			</Link>
		)
	} else {
		return(
			<a href={props.url} target="_blank" className="sectionLink" onMouseOver={props.hoverOver} onMouseLeave={props.hoverRelease}>
				<span className="heading__xl heading__heading-font heading__caps font700">{props.title}</span>
			</a>
		)
	}
}

export default withRouter(SectionLink);