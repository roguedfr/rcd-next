const withSass = require('@zeit/next-sass')

module.exports = withSass({
	webpack: function (config) {
    config.module.rules.push({
      test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 100000,
          name: '[name].[ext]'
        }
      }
    })
    return config
	}
})

const sitemap = require('nextjs-sitemap-generator');  
sitemap({  
  baseUrl: '<your_website_base_url>',  
  pagesDirectory: __dirname + "/pages",  
  targetDirectory : 'static/'  
});