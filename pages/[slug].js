import Head from 'next/head';
import Link from 'next/link';
import fetch from "isomorphic-unfetch";
import Slider from "react-slick";
import { TweenMax } from "gsap";

import Layout from '../components/Layout';
import SectionLink from '../components/SectionLink';

import AboutPage from '../components/About';
import PortfolioPage from '../components/Portfolio';
import PortfolioList from '../components/PortfolioList';

import ScrollBar from "react-scrollbars-custom";

import "../styles.scss";

class Page extends React.Component{
	render(){
		let content = null

		if (this.props.data[0].template === "portfolio-template.php"){
			content = <PortfolioPage data = {this.props.dataACF} />
		} else if (this.props.data[0].template === "portfolio-list.php"){
			content = <PortfolioList data = {this.props.dataPortfolio} pageInfo = {this.props.data}/>
		} else if (this.props.data[0].template === "about-template.php"){
			content = <AboutPage data = {this.props.data}/>
			    	
		}
		return (
	        <React.Fragment>
	        	<Head>
			    	<title>{this.props.data[0].title.rendered} | Rob Clark - Web Developer &amp; UX Designer, Trowbridge, Wiltshire</title>
			    	<meta name="description" content={this.props.data[0].acf.seo_description}/>
			    	<meta name="robots" content="index, follow"/>
			    	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			    	<meta charSet="UTF-8"/>
			    	<link rel="stylesheet" type="text/css" charSet="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
					<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
			  	</Head>
		    	<Layout/>
		    	{content}
	        </React.Fragment>
	    )
	}
};

Page.getInitialProps = async ({ query }) => {
    const res = await fetch(`https://cms.robclarkdesigner.co.uk/wp-json/wp/v2/pages?slug=${query.slug}`);
    let data = await res.json();
    const resACF = await fetch(`https://cms.robclarkdesigner.co.uk/wp-json/acf/v3/pages/${data[0].id}`);
    const dataACF = await resACF.json();
    const resPortfolio = await fetch(`https://cms.robclarkdesigner.co.uk/wp-json/wp/v2/portfolio`);
    const dataPortfolio = await resPortfolio.json();
    return {data, dataACF, dataPortfolio};
};

export default Page