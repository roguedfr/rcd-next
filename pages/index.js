import Head from 'next/head';
import fetch from 'isomorphic-unfetch';
import ReactFullpage from '@fullpage/react-fullpage';
import Slider from "react-slick";
import { TweenMax } from "gsap";

import Layout from '../components/Layout';
import Contact from '../components/Contact';
import SectionLink from '../components/SectionLink';

import "../styles.scss";

export default class Home extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			introLoad: 0,
			portfolioLoad : 0,
			aboutLoad: 0,
			contactLoad: 0
		}
	}

	componentDidMount(){
		if (this.state.introLoad === 0){
			TweenMax.fromTo('#introDeveloper', 2, { x: "-100%",opacity: 0 }, { x: 0, opacity:1, ease: "sine.out" });
			TweenMax.fromTo('#introDesigner', 2, { x: "100%", opacity: 0}, { x: 0, opacity:1, ease: "sine.out" });
			this.setState({introLoad : 1});
		}
	}


	render(){

		const {home} = this.props


		const settings = {
	      dots: false,
	      arrows:false,
	      infinite: true,
	      fade: true,
	      autoplay: true,
	      speed: 750,
	      autoplaySpeed: 3000,
	      pauseOnHover: false,
	    };

	    const aboutHover = () => {
	    	TweenMax.to('#aboutImage', 1, { scale: 1.2, ease: "sine.out", transformOrigin:"50% 50% 50%" });
	    }
	    const aboutRelease = () => {
	    	TweenMax.to('#aboutImage', 1, { scale: 1, ease: "sine.out", transformOrigin:"50% 50% 50%" });
	    }
	    const portfolioHover = () => {
	    	TweenMax.to('.carouselItem', 1, { scale: 1.2, ease: "sine.out", transformOrigin:"50% 50% 50%" });
	    }
	    const portfolioRelease = () => {
	    	TweenMax.to('.carouselItem', 1, { scale: 1, ease: "sine.out", transformOrigin:"50% 50% 50%" });
	    }

	    const moveDown = () => {
	    	fullpage_api.moveSectionDown();
	    }


		return (
		    <React.Fragment>
			    <Head>
			    	<title>Rob Clark - Front End Developer &amp; UX Designer, Trowbridge, Wiltshire</title>
			    	<meta name="description" content={home.acf.seo_description}/>
			    	<meta name="robots" content="index, follow"/>
			    	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			    	<meta charSet="UTF-8"/>
			    	<link rel="stylesheet" type="text/css" charSet="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
					<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
			  	</Head>
		    	<Layout/>
		    	<ReactFullpage
		    	loopBottom = {true}
		    	navigation = {true}
		    	onLeave={(origin, destination, direction) => {
			      	if (this.state.portfolioLoad === 0 && origin.index == 0 && direction =='down'){
				      	TweenMax.fromTo('#portfolioContainer', 2, { opacity: 0, }, { opacity: 1, ease: "sine.out" }).delay(.75);
		    			TweenMax.fromTo('#portfolioAlt', 1.75, { x: '100%' }, { x: '0%', ease: "power2.inOut" });
		    			this.setState({portfolioLoad : 1});
	    			} else if (this.state.aboutLoad === 0 && origin.index == 1 && direction =='down'){
				      	TweenMax.fromTo('#aboutContainer', 2, { opacity: 0, }, { opacity: 1, ease: "sine.out" }).delay(.75);
		    			TweenMax.fromTo('#aboutAlt', 1.75, { x: '100%' }, { x: '0%', ease: "power2.inOut" });
		    			this.setState({aboutLoad : 1});
	    			} else if (this.state.contactLoad === 0 && origin.index == 2 && direction =='down'){
	    				TweenMax.fromTo('#contactContainer', 2, { opacity: 0, }, { opacity: 1, ease: "sine.out" });
		    			TweenMax.fromTo('#contactAlt', 2, { opacity: 0, }, { opacity: 1, ease: "sine.out" }).delay(.75);
		    			this.setState({contactLoad : 1});
	    			}
			    }}
		          render={comp =>
			    	<ReactFullpage.Wrapper>
				    	<section className="grid-container fluid Section__main section">
				    		<div className="align-stretch grid-x h100">
				    			<div id="introDeveloper" className="cell xlarge-6 small-12 Section__Primary grid-x align-middle align-center bgImage__cover" style={{backgroundImage:'url('+ home.acf.developer_block.background_image + ')'}}>
				    				<div className="cell small-6">
				    					<h1 className="heading heading__xl heading__caps">{home.acf.developer_block.content}</h1>
				    				</div>
				    			</div>
				    			<div id="introDesigner" className="cell xlarge-6 small-12 Section__Dark grid-x align-middle align-center bgImage__cover" style={{backgroundImage:'url('+ home.acf.designer_block.background_image + ')'}}>
				    				<div className="cell small-6">
				    					<h1 className="heading heading__xl heading__primary heading__caps">{home.acf.designer_block.content}</h1>
				    					<div className="scroll-chevron" onClick={() => moveDown()}></div>
				    				</div>
				    			</div>
				    		</div>
				    	</section>
				    	<section className="grid-container fluid Section__main section">
				    		<div className="align-stretch grid-x h100">
				    			<div className="cell xlarge-6 small-12 small-order-2 xlarge-order-1 grid-x portfolio_carousel h50" id="portfolioContainer">
				    				<Slider {...settings}>
				    					{home.acf.portfolio_carousel
				    						.map((portfolioKey, i) => {
				    							return(
				    								<div key={i} className="carouselItem"><img src={portfolioKey.image_url}/></div>
				    							)
				    						})}
				    				</Slider>
				    				<SectionLink
				    					title={home.acf.portfolio_link_title}
				    					url={home.acf.portfolio_link}
				    					hoverOver={portfolioHover}
				    					hoverRelease={portfolioRelease}
				    					internal = "true"
				    				/>
				    			</div>
				    			<div className="cell xlarge-6 small-12 small-order-1 xlarge-order-2 Section__Primary grid-x align-middle align-center h50" id="portfolioAlt">
				    				<div className="cell small-6 text-center">
				    					<h2 className="heading heading__xxl heading__caps">Portfolio</h2>
				    				</div>
				    			</div>
				    		</div>
				    	</section>
				    	<section className="grid-container fluid Section__main section">
				    		<div className="align-stretch grid-x h100">
				    			<div className="cell xlarge-6 small-12 small-order-2 xlarge-order-1 grid-x align-middle align-center h50" id="aboutContainer">
				    				<img src={home.acf.about_image} id="aboutImage"/>
				    				<SectionLink
				    					title={home.acf.portfolio_link_title}
				    					url={home.acf.about_link}
				    					hoverOver={aboutHover}
				    					hoverRelease={aboutRelease}
				    					internal = "true"
				    				/>
				    			</div>
				    			<div className="cell xlarge-6 small-12 small-order-1 xlarge-order-2 Section__Dark grid-x align-middle align-center h50" id="aboutAlt">
				    				<div className="cell small-6 text-center">
				    					<h2 className="heading heading__xxl heading__caps heading__primary">About</h2>
				    				</div>
				    			</div>
				    		</div>
				    	</section>
				    	<section className="grid-container fluid Section__main section">
				    		<div className="align-stretch grid-x h100">
				    			<div className="cell xlarge-6 small-12 small-order-2 xlarge-order-1 grid-x align-middle align-center Section__Primary h50" id="contactContainer">
				    				<div className="cell small-9 medium-6 text-left">
				    					<Contact/>
				    				</div>
				    			</div>
				    			<div className="cell xlarge-6 small-12 small-order-1 xlarge-order-2 Section__Dark grid-x align-middle align-center h50 show-for-large" id="contactAlt">
				    				<div className="cell small-6 text-center">
				    					<h2 className="heading heading__xxl heading__caps heading__primary">Contact</h2>
				    				</div>
				    			</div>
				    		</div>
				    	</section>
			    	</ReactFullpage.Wrapper>
		          }
		        />
		    </React.Fragment>
		  )
	}

}


Home.getInitialProps = async function(context) {
  const res = await fetch(`https://cms.robclarkdesigner.co.uk/wp-json/acf/v3/pages/24`);
  const home = await res.json();
  
  
  return { home };
};