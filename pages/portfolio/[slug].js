import Head from 'next/head';
import Link from 'next/link';
import Router from 'next/router'
import fetch from "isomorphic-unfetch";
import Slider from "react-slick";
import { TweenMax } from "gsap";
import ScrollBar from "react-scrollbars-custom";

import ReactFullpage from '@fullpage/react-fullpage';

import Layout from '../../components/Layout';
import SectionLink from '../../components/SectionLink';

import "../../styles.scss";

class Portfolio extends React.Component{

	render(){

	const settings = {
      dots: false,
      arrows:false,
      infinite: true,
      fade: true,
      speed: 750,
      autoplay: true,
  	  autoplaySpeed: 4000,
    };

	return (
	        <React.Fragment>
	        	<Head>
			    	<title>{this.props.data[0].title.rendered} | Rob Clark - Front End Developer &amp; UX Designer, Trowbridge, Wiltshire</title>
			    	<meta name="description" content="Rob Clark, Professional Front End Developer and UX Designer in Trowbridge, Wiltshire, near Bath. Building Quality, clean websites as a Front End Developer."/>
			    	<meta name="robots" content="index, follow"/>
			    	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			    	<meta charset="UTF-8" />
			    	<link rel="stylesheet" type="text/css" charSet="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
					<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
			  	</Head>
		    	<Layout/>
			    	<section className="grid-container fluid Section__main section">
			    		<div className="align-stretch grid-x h100">
			    			<div className="cell small-12 large-6 grid-x portfolio_carousel h50">
			    				<Slider {...settings}>
			    					{this.props.data[0].acf.image_gallery
			    						.map((imageKey, i) => {
			    							return(
			    								<React.Fragment>
			    								<div key={i} className="carouselItem"><img src={imageKey.image}/></div>
			    								{this.props.data[0].acf.website_link ? <SectionLink
							    					title="View"
							    					url={this.props.data[0].acf.website_link}
							    					hoverOver=""
							    					hoverRelease=""
							    				/> : null}
							    				</React.Fragment>
			    							)
			    						})}
			    				</Slider>
			    			</div>
			    			<div className="cell small-12 large-6 grid-x align-top align-center h50 position-relative">
			    				<div id="backButton" onClick={() => Router.back()}>Return to portfolio</div>
			    				<ScrollBar style={{ width: "100%", height: "100%" }}>
			    					<div className="small-12 grid-x cell align-center">
					    				<div className="cell small-8 text-center pt5 grid-x ">
					    					<h2 className="heading heading__xxl heading__caps cell small-12">{this.props.data[0].title.rendered}</h2>
					    					<hr className="ruler__dark cell small-12"/>
					    					<h4 className="heading heading__sm cell small-12 pb2 pt1">{this.props.data[0].acf.description}</h4>
					    					{this.props.data[0].acf.information
					    						.map(informationKey => {
					    							if(informationKey.acf_fc_layout ==="content"){
					    								return(
					    									<div className="cell small-12 pb2" dangerouslySetInnerHTML={{__html:informationKey.content}}/>
					    								)
					    							} if(informationKey.acf_fc_layout ==="button"){
					    								return(
					    									<React.Fragment>
					    										<div className="cell small-12 pb2" >
					    											<a href={informationKey.link} className="button hollow expanded secondary">{informationKey.title}</a>
					    										</div>
					    									</React.Fragment>
					    								)
					    							}
					    						})
					    					}
					    				</div>
				    				</div>
			    				</ScrollBar>
			    			</div>
			    		</div>
			    	</section>
	        </React.Fragment>
	    )
	}
};

Portfolio.getInitialProps = async ({ query }) => {
    const res = await fetch(`https://cms.robclarkdesigner.co.uk/wp-json/wp/v2/portfolio?slug=${query.slug}`);
    let data = await res.json();
    return {data};
};

export default Portfolio